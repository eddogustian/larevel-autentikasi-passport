<p align="center"><a href="https://laravel.com" target="_blank"><img src="https://raw.githubusercontent.com/laravel/art/master/logo-lockup/5%20SVG/2%20CMYK/1%20Full%20Color/laravel-logolockup-cmyk-red.svg" width="400"></a></p>

![alt text](https://gitlab.com/eddogustian/larevel-autentikasi-passport/-/blob/master/public/img/laravel-passport.png?raw=true)

## Step #1 
    - [Install Package Passport Menggunakan Composer].
    - [Silahkan ketik command di bawah ini untuk menginstall package passport di Laravel:]
        composer require laravel/passport
## Step #2 - Konfigurasi Package Passport
    - [Agar package ini bisa dijalankan, maka kita perlu mendaftarkannya dulu di config/app.php. Tambahkan:]
    Laravel\Passport\PassportServiceProvider::class, 
## Step #3 - Memodifikasi Model User
    - [Buka User.php di dalam direktori app/Models/User.php, kemudian modifikasi :](https://gitlab.com/eddogustian/larevel-autentikasi-passport/-/blob/master/app/Models/User.php).
   
## Step #4 - Modifikasi AuthServiceProvider
    - [Silahkan buka AuthServiceProvider.php di direktori app/Providers/AuthServiceProviders.php kemudian modifikasi seperti ini:]
## Step #5 - Modifikasi Auth
    - [Cari kode di bawah ini di direktori config/auth.php:]
    'guards' => [
        'web' => [
            'driver' => 'session',
            'provider' => 'users',
        ],
        
        'api' => [
            'driver' => 'token',
            'provider' => 'users',
        ],
    ],  
    Kemudian ubah menjadi:
    'guards' => [
        'web' => [
            'driver' => 'session',
            'provider' => 'users',
        ],

        'api' => [
            'driver' => 'passport',//instead of token 
            'provider' => 'users',
            'hash' => false,
        ],
    ],
## Step #6 - Membuat Controller User
    - [Ketik command di bawah ini:]
        php artisan make:controller Api/UserController

   - [Kemudian modifikasi UserController.php di dalam direktori app/Http/Controllers/Api/UserController.php:]

## Step #7 - Membuat Route
    - [By default, Laravel sudah menyedian 2 route, yaitu web dan api. Nah, dalam proses ini kita akan menggunakan api.php. Silahkan buka direktori routes/api.php kemudian tambahkan:]
        Route::post('login', 'API\UserController@login');
        Route::post('register', 'API\UserController@register');

        Route::group(['middleware' => 'auth:api'], function(){
            Route::get('user/detail', 'Api\UserController@details');
            Route::post('logout', 'Api\UserController@logout');
        }); 
Sampai di sini proses pembuatan API menggunakan package Passport sudah selesai. Kini saatnya testing!